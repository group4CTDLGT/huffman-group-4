#include "Header.h"

void CFiles::ReadFilesName(const char* parent) // Get files's name
{
	WIN32_FIND_DATAA fd;
	LARGE_INTEGER filesize;

	char folder[MAX_PATH];
	sprintf(folder, "%s\\*.*", parent);

	HANDLE hFind = FindFirstFileA(folder, &fd);

	if (hFind != INVALID_HANDLE_VALUE)
	{
		do {
			if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (strcmp(fd.cFileName, ".") && strcmp(fd.cFileName, ".."))
				{
					char child[MAX_PATH];
					sprintf(child, "%s\\%s", parent, fd.cFileName);
					ReadFilesName(child);
				}
			}
			else
			{
				char buff[1000];
				int n;
				sprintf(buff, "%s\\%s", parent, fd.cFileName);
				char * temp = new char[strlen(fd.cFileName) + 1];
				strcpy(temp, fd.cFileName);
				this->m_Files.push_back(temp);
			}
		} while (FindNextFileA(hFind, &fd));
		FindClose(hFind);
	}
	fstream f;
	m_FileSize = new long long[m_Files.size()];
	for (int i = 0; i < m_Files.size(); i++)
		m_FileSize[i] = 0;
	for (int i = 0; i < m_Files.size(); i++)
	{
		char buff[1000];
		sprintf(buff, "%s\\%s", m_LinkIn, m_Files[i]);
		f.open(buff, ios::in | ios::binary);
		f.seekg(0, f.end);
		m_FileSize[i] = f.tellg();
		f.seekg(0, f.beg);
		f.close();
		f.clear();
	}
}

void CFiles::GetFreq() // get frequency of each character 
{
	fstream f;
	unsigned char cha;
	for (int i = 0; i < m_Files.size(); i++)
	{
		char buff[1000];
		sprintf(buff, "%s\\%s", m_LinkIn, m_Files[i]);
		f.open(buff, ios::in | ios::binary);
		for (int j = 0; j < m_FileSize[i]; j++)
		{
			f.read((char*)&cha, 1);
			m_Table[cha]++;
		}
		f.seekg(0, f.beg);
		f.close();
		f.clear();
	}
}